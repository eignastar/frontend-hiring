import { useState } from 'react';

// components
import {Header} from './components/Header';
import {Questionnaire} from './components/Questionnaire';
import {Footer} from './components/Footer/index.js';

// style
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  // questionnaire response
  const [response, setResponse] = useState({}); 

  return (
    <div>
      <Header />
      <Questionnaire 
        saveResponse={setResponse}
      />
      <Footer 
        response={response}
        clearResponse={setResponse}
      />
    </div>
  );
}

export default App;
