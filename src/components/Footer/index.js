import { useState, useEffect } from 'react';
import {SubmittedModal} from './SubmittedModal';

export const Footer = (props) => {
    const [responses, setResponses] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [modalData, setModalData] = useState({});

    // saves previous questionnaire submissions
    useEffect(() => {
        if (Object.keys(props.response).length !== 0) {
            setResponses(responses.concat([props.response]))
            props.clearResponse({})
        }
    }, [responses, props]);

    // handles opening/closing submission modal
    const toggleModal = (i) => {
        setShowModal(!showModal)
        if (!showModal) {
            setModalData({...responses[i], i: i+1})
        }
        else setModalData({})
    }

    // produces list of past submissions to be displayed
    const getSubmissions = () => {
        if (responses.length === 0) 
            return "No submissions received yet"
        else {
            return responses.map((response, i) => {
                return (
                    <li key={i} onClick={() => {toggleModal(i)}}>
                        Submission {i+1}
                    </li>
                );
            })
        }
    }

    return (
        <div className='footer'>
            <h2 className='footer-head'> 
                Previous Submissions: 
            </h2>
            <div className='footer-body'>
                <ul className='submissions'>
                    {getSubmissions()}
                </ul>
            </div>
            <SubmittedModal 
                show={showModal} 
                closeModal={toggleModal} 
                response={modalData}
            />
        </div>
    );
}