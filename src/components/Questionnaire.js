import { useState } from "react";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

export const Questionnaire = (props) => {
  // questionnaire fields
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [description, setDescription] = useState("");
  const [logo, setLogo] = useState("");
  const [logoSrc, setLogoSrc] = useState("");
  const [services, setServices] = useState([]);
  const [budget, setBudget] = useState("");
  const [deadline, setDeadline] = useState("");

  // data validity checks
  const [validName, setValidName] = useState(true);
  const [validEmail, setValidEmail] = useState(true);
  const [validPhone, setValidPhone] = useState(true);
  const [validCompanyName, setValidCompanyName] = useState(true);
  const [validDescription, setValidDescription] = useState(true);
  const [validLogo, setValidLogo] = useState(true);
  const [validServices, setValidServices] = useState(true);
  const [validBudget, setValidBudget] = useState(true);
  const [validDeadline, setValidDeadline] = useState(true);

  // updates services state with selected/unselected services
  const selectedServices = (e, service) => {
    if (e.target.checked) setServices(services.concat([service]));
    else {
      let index = services.indexOf(service);
      let servicesCopy = services.slice();
      servicesCopy.splice(index, 1);
      if (index >= 0) setServices(servicesCopy);
    }
  };

  // handles showing preview of uploaded image
  const uploadedImg = (e) => {
    const preview = document.querySelector('img');
    var file = e.target.files[0];
    var reader = new FileReader();

    reader.onload = function(e) {
      preview.src = reader.result;
      setLogoSrc(preview.src)
      preview.style.display = "inline-block";
    }

    reader.readAsDataURL(file);
  }

  // validates questionnaire fields
  const validForm = (response) => {
    let valid = true
    for (const [key, val] of Object.entries(response)) {
      switch (key) {
        case "name":
          if(!(/^[a-z ,.'-]+$/i.test(val))) {
            setValidName(false)
            valid = false
          }
          break;
        case "email":
          if(!(/^\S+@\S+$/.test(val))) {
            setValidEmail(false)
            valid = false
          }
          break;
        case "phone":
          if(!(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/.test(val))) {
            setValidPhone(false)
            valid = false
          }
          break;
        case "companyName":
          if(!(/^[a-z ,.'-]+$/i.test(val))) {
            setValidCompanyName(false)
            valid = false
          }
          break;
        case "description":
          if(!val.length) {
            setValidDescription(false)
            valid = false
          }
          break;
        case "logo":
          if(!val.length) {
            setValidLogo(false)
            valid = false
          }
          break;
        case "services":
          if(!val.length) {
            setValidServices(false)
            valid = false
          }
          break;
        case "budget":
          if(!val.length) {
            setValidBudget(false)
            valid = false
          }
          break;
        case "deadline":
          if(!val.length) {
            setValidDeadline(false)
            valid = false
          }
          break;
        default:
      }
    }

    return valid
  };

  // handles clearing form after submission
  const clearForm = () => {
    setName("");
    setEmail("");
    setPhone("");
    setCompanyName("");
    setDescription("");
    setLogo("");
    setLogoSrc("");
    const preview = document.querySelector('img');
    preview.src = '';
    preview.style.display = "none";
    setServices([]);
    setBudget("");
    setDeadline("");

    setValidName(true);
    setValidEmail(true);
    setValidPhone(true);
    setValidCompanyName(true);
    setValidDescription(true);
    setValidLogo(true);
    setValidServices(true);
    setValidBudget(true);
    setValidDeadline(true);
  }

  // saves questionnaire data upon submission
  const saveForm = (e) => {
    e.preventDefault();
    let response = {
      name,
      email,
      phone,
      companyName,
      description,
      logo,
      logoSrc,
      services,
      budget,
      deadline,
    };
    if (validForm(response)) {
      props.saveResponse(response);
      clearForm();
    }
  };

  return (
    <div className="form">
      <Form>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridName">
            <Form.Label>Name</Form.Label>
            <Form.Control
              placeholder="John Smith"
              onChange={(e) => {
                setName(e.target.value);
                setValidName(true);
              }}
              value={name}
              isInvalid={!validName}
            />
          </Form.Group>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="smith@web.com"
              onChange={(e) => {
                setEmail(e.target.value);
                setValidEmail(true);
              }}
              value={email}
              isInvalid={!validEmail}
            />
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPhone">
            <Form.Label>Phone Number</Form.Label>
            <Form.Control
              type="tel"
              placeholder="000-000-0000"
              onChange={(e) => {
                setPhone(e.target.value);
                setValidPhone(true);
              }}
              value={phone}
              isInvalid={!validPhone}
            />
          </Form.Group>
        </Row>

        <Form.Group className="mb-3" controlId="formGridCompany">
          <Form.Label>Company Name</Form.Label>
          <Form.Control
            placeholder="Treeline Interactive"
            onChange={(e) => {
              setCompanyName(e.target.value);
              setValidCompanyName(true);
            }}
            value={companyName}
            isInvalid={!validCompanyName}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formGridDescription">
          <Form.Label>Company Description</Form.Label>
          <Form.Control
            as="textarea"
            onChange={(e) => {
              setDescription(e.target.value);
              setValidDescription(true);
            }}
            value={description}
            isInvalid={!validDescription}
          />
        </Form.Group>

        <Form.Group controlId="formFile" className="mb-3">
          <Form.Label>Company Logo</Form.Label>
          <Form.Control
            type="file"
            accept="image/*"
            onChange={(e) => {
              uploadedImg(e);
              setLogo(e.target.value);
              setValidLogo(true);
            }}
            value={logo}
            isInvalid={!validLogo}
          />
        </Form.Group>

        <img className="logo-img" src="" height="200" alt="Logo preview"/>

        <Form.Group className="mb-3" id="formGridCheckbox">
          <Form.Label>
            What services are you looking for? Check all that apply.
          </Form.Label>
          <Form.Check
            checked={services.includes(1)}
            type="checkbox"
            label="Redesign current website"
            onChange={(e) => {
              selectedServices(e, 1);
              setValidServices(true);
            }}
            isInvalid={!validServices}
          />
          <Form.Check
            checked={services.includes(2)}
            type="checkbox"
            label="Create new website"
            onChange={(e) => {
              selectedServices(e, 2);
              setValidServices(true);
            }}
            isInvalid={!validServices}
          />
          <Form.Check
            checked={services.includes(3)}
            type="checkbox"
            label="Website maintenance"
            onChange={(e) => {
              selectedServices(e, 3);
              setValidServices(true);
            }}
            isInvalid={!validServices}
          />
          <Form.Check
            checked={services.includes(4)}
            type="checkbox"
            label="Content marketing"
            onChange={(e) => {
              selectedServices(e, 4);
              setValidServices(true);
            }}
            isInvalid={!validServices}
          />
        </Form.Group>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridState">
            <Form.Label>What is your budget?</Form.Label>
            <Form.Select
              onChange={(e) => {
                setBudget(e.target.value);
                setValidBudget(true);
              }}
              value={budget}
              isInvalid={!validBudget}
            >
              <option></option>
              <option>Up to $5,000</option>
              <option>$5,000 - $15,000</option>
              <option>$15,000 - $25,000</option>
              <option>$25,000 - $35,000</option>
              <option>$35,000+</option>
            </Form.Select>
          </Form.Group>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridState">
            <Form.Label>When would you like the website to launch?</Form.Label>
            <Form.Control
              placeholder="(ie. by the end of the year)"
              onChange={(e) => {
                setDeadline(e.target.value);
                setValidDeadline(true)
              }}
              value={deadline}
              isInvalid={!validDeadline}
            />
          </Form.Group>
        </Row>

        <div className="form-button">
          <Button
            variant="dark"
            type="submit"
            onClick={(e) => {
              saveForm(e);
            }}
          >
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
};
