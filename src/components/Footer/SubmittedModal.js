import Modal from "react-bootstrap/Modal";

// pairs checked box to service value
const service_val = {
    1: 'Redesign current website',
    2: 'Create new website',
    3: 'Website maintenance',
    4: 'Content marketing',
}

export const SubmittedModal = (props) => {
  const {name, email, phone, companyName, description, logo, logoSrc, services, budget, deadline} = props.response;

  // get filename
  let filename = logo ? logo.split('\\') : []
  filename = filename.length ? filename[filename.length-1] : ''

  // gets checked services
  const getServices = () => {
      if (services) {
        return services.map((service, i) => {
            return <li key={i}>{service_val[service]}</li>
        })
    }
  }

  return (
    <Modal show={props.show} onHide={props.closeModal}>
      <Modal.Header closeButton>
        <Modal.Title>Submission {props.response.i}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
          <div><strong>Name:</strong> {name}</div>
          <div><strong>Email:</strong> {email}</div>
          <div><strong>Phone Number:</strong> {phone}</div>
          <div><strong>Company Name:</strong> {companyName}</div>
          <div><strong>Company Description:</strong> {description}</div>
          <div><strong>Company Logo:</strong> {filename}</div>
          <img src={logoSrc} height="200" alt="Logo preview"/>
          <div><strong>Desired Services:</strong> 
            <ul>
                {getServices()}
            </ul>
          </div>
          <div><strong>Budget:</strong> {budget}</div>
          <div><strong>Desired Launch Date:</strong> {deadline}</div>
      </Modal.Body>
    </Modal>
  );
};
