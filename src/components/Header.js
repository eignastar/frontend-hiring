export const Header = (props) => {
    return (
        <div>
            <h1>
                Web Design Questionnaire
            </h1>
            <p className='header-description'>
                Thank you for your interest in our services! To get started,
                please fill out this form so we can learn a bit more about you
                and what kind of website you're looking to build.
            </p>
        </div>
    );
}